//
//  TagCell.swift
//  TagFlowLayout
//
//  Created by Emanuel Thieme on 29.08.17 | KW 35.
//  Copyright © 2017 Emanuel Thieme. All rights reserved.
//

import UIKit

class TagCell: UICollectionViewCell {
    
    @IBOutlet weak var tagName: UILabel!
    @IBOutlet weak var tagNameMaxWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var tagImg: UIImageView!
    
    override func awakeFromNib() {
        self.layer.cornerRadius = self.frame.size.width / 4.5
        self.tagNameMaxWidthConstraint.constant = UIScreen.main.bounds.width - 8 * 2 - 8 * 2
    }
}
