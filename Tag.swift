//
//  Tag.swift
//  TagFlowLayout
//
//  Created by Emanuel Thieme on 29.08.17 | KW 35.
//  Copyright © 2017 Emanuel Thieme. All rights reserved.
//

import Foundation

open class Tag: NSObject {
    
    open var name : String? = ""
    open var selected = true
    
    public override init() {
    }
    
    public init(name: String, selected: Bool = true) {
        self.name = name
        self.selected = selected
    }

}
