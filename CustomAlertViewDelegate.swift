//
//  CustomAlertViewDelegate.swift
//  CustomAlertView
//
//  Created by Emanuel Thieme on 06.09.17 | KW 35.
//  Copyright © 2017 Emanuel Thieme. All rights reserved.
//

protocol CustomAlertViewDelegate: class {
    func okButtonTapped(textFieldValue: String)
    func cancelButtonTapped()
}
