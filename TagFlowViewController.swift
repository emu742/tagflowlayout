//
//  TagFlowViewController.swift
//  TagFlowLayout
//
//  Created by Emanuel Thieme on 29.08.17 | KW 35.
//  Copyright © 2017 Emanuel Thieme. All rights reserved.
//

import UIKit

open class TagFlowViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var sizingCell: TagCell?
    open weak var delegate : TagFlowLayoutDelegate?
    
    open var tags = [Tag]()
    open var data : [Tag] {
        get {
            return self.tags
        }
        set(value) {
            self.tags = value
        }
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        let tagNib = UINib(nibName: "TagCell", bundle: Bundle(for: TagFlowViewController.self))
        self.collectionView.register(tagNib, forCellWithReuseIdentifier: "TagCell")
        let addNib = UINib(nibName: "AddCell", bundle: Bundle(for: TagFlowViewController.self))
        self.collectionView.register(addNib, forCellWithReuseIdentifier: "AddCell")
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.backgroundColor = .lightGray
        self.sizingCell = (tagNib.instantiate(withOwner: nil, options: nil) as NSArray).firstObject as! TagCell?
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tags.count + 1
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == tags.count {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddCell", for: indexPath) as! AddCell
            cell.addImg.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 4)
            return cell
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TagCell", for: indexPath) as! TagCell
            self.configureCell(cell, forIndexPath: indexPath)
            return cell
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.row == tags.count { return CGSize(width: 56, height: 28) }
        self.configureCell(self.sizingCell!, forIndexPath: indexPath)
        return self.sizingCell!.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if (indexPath.row == tags.count) { delegate?.tagFlowLayoutAdd!(); addKeyword(); return}
        collectionView.deselectItem(at: indexPath, animated: false)
        let tag = tags[indexPath.row]
        tag.selected = !tag.selected
        
        if tag.selected {
            delegate?.tagFlowLayoutButtonTurnedOn?(tag.name)
        } else {
            delegate?.tagFlowLayoutButtonTurnedOff?(tag.name)
        }
        delegate?.tagFlowLayoutButtonChanged?(tag.name)
        delegate?.tagFlowLayoutButtonTapped(tags, sender: self)
        
        self.collectionView.reloadData()
    }
    
    func configureCell(_ cell: TagCell, forIndexPath indexPath: IndexPath) {
        if (tags.count > indexPath.row)
        {
            let tag = tags[indexPath.row]
            cell.tagName.text = tag.name
            cell.tagName.textColor = tag.selected ? .pink : .darkGray
            
            cell.backgroundColor = tag.selected ? .white : .lightGray
            cell.layer.borderColor = tag.selected ? UIColor.clear.cgColor : UIColor.darkGray.cgColor
            cell.layer.borderWidth = tag.selected ? 0 : 1
            
            cell.tagImg.tintColor = tag.selected ? .pink : .darkGray
            cell.tagImg.transform = CGAffineTransform(rotationAngle: (tag.selected ? CGFloat.pi : CGFloat.pi / 4))
        }
    }
    
    // Keyword Hinzufügen
    
    func addKeyword() {
        let addNib = UINib(nibName: "CustomAlertView", bundle: Bundle(for: TagFlowViewController.self))
        let customAlert = addNib.instantiate(withOwner: nil, options: nil)[0] as! CustomAlertView
        customAlert.messageLabel.text = activeTagsToString(seperated: ", ")
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self
        self.present(customAlert, animated: true, completion: nil)
    }
    
    internal func activeTagsToString(seperated with: String = " ") -> String {
        var array = [String]()
        self.tags.forEach(){
            $0.selected ? array += [$0.name!] : nil
        }
        return array.joined(separator: with)
    }
    
    internal func stringArrayToTags(_ strings: [String]) -> [TagFlowLayout.Tag] {
        return strings.map {
            let tag = Tag()
            tag.name = $0.trimmingCharacters(in: NSCharacterSet.alphanumerics.inverted)
            return tag
            }.filter { !($0.name?.isEmpty)! }
    }
    
    internal func lastItem() -> IndexPath
    {
        return IndexPath(row: collectionView.numberOfItems(inSection: 0) - 1, section: 0)
    }
    
    
    // Onboarding
    
    var closureSaver: ((_ cell:UICollectionViewCell?) -> Void)?
    open func addKeywordSourceView(completion:@escaping (_ cell:UICollectionViewCell?)->Void){
        guard let cell = self.collectionView.cellForItem(at: lastItem()) else {
            self.collectionView.scrollToItem(at: lastItem(), at: .right, animated: true)
            closureSaver = completion
            return
        }
        completion(cell)
    }
    
    public func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        closureSaver?(self.collectionView.cellForItem(at: lastItem()))
    }
}


extension TagFlowViewController: CustomAlertViewDelegate {
    
    func okButtonTapped(textFieldValue: String) {
        if !textFieldValue.isEmpty
        {
            print("TextField has value: \(textFieldValue)")
            
            let newTags = self.stringArrayToTags(textFieldValue.components(separatedBy: " "))
            self.data += newTags
            self.collectionView.reloadData()
            self.delegate?.tagFlowLayoutKeywordsAdded?(newTags)
        }
    }
    
    func cancelButtonTapped() {
        print("cancelButtonTapped")
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    //        let alert = UIAlertController(title: "Suchbegriffe hinzufügen", message: activeTagsToString(seperated: ", "), preferredStyle: UIAlertControllerStyle.alert)
    //        let add = UIAlertAction(title: "OK", style: .default) { (alertAction) in
    //            if let text = alert.textFields?.first?.text
    //            {
    //                let newTags = self.stringArrayToTags(text.components(separatedBy: " "))
    //                self.data += newTags
    //                self.collectionView.reloadData()
    //                self.delegate?.tagFlowLayoutKeywordsAdded?(newTags)
    //            }
    //        }
    //        let cancel = UIAlertAction(title: "Abbrechen", style: .cancel, handler: nil)
    //        alert.addTextField { (textField) in
    //            textField.placeholder = "Trenne die Begriffe mit Leerzeichen"
    //        }
    //        alert.addAction(add)
    //        alert.addAction(cancel)
    //        self.present(alert, animated:true, completion: nil)
    
}

