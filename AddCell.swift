//
//  AddCell.swift
//  TagFlowLayout
//
//  Created by Emanuel Thieme on 08.03.18 | KW 10.
//  Copyright © 2018 Emanuel Thieme. All rights reserved.
//

import UIKit

class AddCell: UICollectionViewCell {
    
    @IBOutlet weak var addImg: UIImageView!
    
    override func awakeFromNib() {
        addImg.layer.cornerRadius = self.frame.size.width / 2
        addImg.tintColor = .pink
    }
}

