//
//  TagFlowLayoutDelegate.swift
//  CamFind
//
//  Created by Emanuel Thieme on 12.09.17 | KW 37.
//  Copyright © 2017 Emanuel Thieme. All rights reserved.
//

import Foundation

@objc public protocol TagFlowLayoutDelegate : class {
    func tagFlowLayoutButtonTapped(_ tag: [Tag], sender: TagFlowViewController)
    
    @objc optional func tagFlowLayoutButtonTurnedOn(_ tag: String?)
    @objc optional func tagFlowLayoutButtonTurnedOff(_ tag: String?)
    @objc optional func tagFlowLayoutButtonChanged(_ tag: String?)
    @objc optional func tagFlowLayoutAdd()
    @objc optional func tagFlowLayoutKeywordsAdded(_ new: [Tag]?)
}
